FROM alpine:3.16 AS base
COPY start.sh /
RUN mkdir server \
    && chmod +x start.sh
WORKDIR /server
RUN /start.sh

FROM eclipse-temurin:8u332-b09-jre-jammy AS server-install
COPY --from=base /server /server/
WORKDIR /server
COPY server.properties /server/
RUN java -jar installer.jar --installServer \
    && rm -rf installer* \
    && ln -s forge-*.jar server.jar

FROM eclipse-temurin:8u332-b09-jre-jammy as main

COPY run-server.sh /
COPY --from=server-install /server /server/

RUN adduser --system --group forge && \
    chmod +x run-server.sh

WORKDIR /server

VOLUME [ "/server" ]
EXPOSE 25565

# Defaults
ENV EULA=false
ENV MINIMUM_RAM=2G
ENV MAXIMUM_RAM=6G

ENTRYPOINT /run-server.sh
