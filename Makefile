upload-package:
	curl --header "PRIVATE-TOKEN: ${TOKEN}" --upload-file RLCraft-v2.9.1c.zip "https://gitlab.com/api/v4/projects/38266496/packages/generic/rlcraft/2.9.1c/rlcraft.zip"

build:
	docker build -t astarivi/rlcraft-server:2.9.1c-v1 .

push:
	docker push astarivi/rlcraft-server:2.9.1c-v1